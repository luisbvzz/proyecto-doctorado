#!/bin/bash

rm Convertidas/*.png

i=0
for filename in Imagenes/*.jpeg
do 
    convert $filename -monochrome Convertidas/$i.png
    python ParticulasNegras.py Convertidas/$i.png >> Datos/particulas$i.txt
    i=$((i+1))
done
