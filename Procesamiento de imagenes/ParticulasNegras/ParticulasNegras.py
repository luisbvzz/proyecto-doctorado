from PIL import Image
from random import randint
from sys import argv
blanco = 255
negro = 0


input=argv[1]

im = Image.open(input)

pixels = im.load() # cargar imagen img
# w = width (ancho)
# h = height (alto)
w, h = im.size 


# Poner en un set todos los pixeles de la imagenen en pendientes
pendientes = set() 
for y in xrange(h):
    for x in xrange(w):
        pendientes.add((x, y))

# Regresa verdadero cuando el pixel esta pendiente
# pendientes y si esta dentro de la imagen
# y si es blanco
def sirve(x, y):
    global pixels, negro, pendientes, w, h
    if (x, y) not in pendientes:
        return False
    if x>= 0 and x < w and y >= 0 and y < h:
        return pixels[x, y] == negro
    else:
        return False

def dfs(x, y):
    global pendientes
    cola = [(x, y)] # La cola que se tomara para seleccionar a los elementos
    seleccion = set() # Elemento seleccionado se guarda en un set
    while len(cola) > 0:
        (x, y) = cola.pop(0) # Eliminar el elemento actual (x,y) de la cola
        if (x, y) in pendientes: # Si el pixel esta pendiente
            pendientes.remove((x, y)) # Lo removemos de los pendientes
            seleccion.add((x, y))   # Lo annadimos a la seleccion
            #### Buscamos para sus vecinos siempre y cuando sean blancos
            for dx in xrange(-1,2): 
                for dy in xrange(-1,2):
                    vx = x + dx
                    vy = y + dy
                    if sirve(vx, vy):
                        cola.append((vx, vy))
    return seleccion # regresar la seleccion echa

Particulas = list()
mayor = 0
fondo = None # inicializar fondo
for y in xrange(h): # para cada pixel en height
    for x in xrange(w): # para cada pixel en width
        #print "Entro"
        if sirve(x, y): # si sirve
            
            zona = dfs(x, y) # Se tiene una zona (particula o fondo)
            Particulas.append(zona) #agregar la zona a las particulas

total = w * h * 1.0 # calcular el tamanno
prop = max(w, h) #Calcular si el ancho o el largo es mayor
for p in Particulas: #Para cada particula
    xtot = 0.0
    ytot = 0.0
    for (x, y) in p:
        xtot += x # sumar los valores de x para calcular el porcentaje que se encuentra del ancho
        ytot += y # sumar los valores de x para calcular el porcentaje que se encuentra de la altura
    n = len(p) # Numero de particula
    print n / total, (xtot / n) / prop, (ytot / n) / prop
    # (n / total) normalizar tamanno de particula
    # ((xtot / n) / prop) porcentaje del ancho de la imagen donde se encuentra la particula
    # ((ytot / n) / prop) porcentaje del alto de la imagen donde se encuentra la particula
