#Lectura de datos experimentales
#Columnas:
#Bins,1min,2min, 3min, 4min, 5min, 6min, 7min, 10min
datos = read.csv("Distribuciones.dat",sep=" ", header=FALSE)

print(length(datos))

E = c("Cr 20 min", "Cr 40 min", "Fe 20 min", "Fe 40 min", "Ni 20 min", "Ni 40 min", "Zn 20 min", "Zn 40 min")# Experimentos 


bin = datos$V1
minutos = c(1,2,3,4,5,6,7,10)

indice = 2
for (test in E){
    PVALUES = numeric()
    Ps = numeric()
    Pmejores = numeric()
    Psmejores = numeric()
    Minutos = numeric()
    #cat(indice, indice + 7, "\n")
    m = 1
    for (i  in indice:(indice+7)) {
        #print(i)
        datosLab = t(datos[i])
        datosLab = datosLab/sum(datosLab) #normalizar los datos

        # Quitar los datos que estan en 15, 25, etc
        # y sumarselos a 20 (15), 30 (25), etc
        for (j in 1:length(datosLab) - 1){
            if (j %% 2 == 0){
                datosLab[j] = NA
            }
            else{
                datosLab[j] = datosLab[j] + datosLab[j+1]
            }

        }
        #print(datosLab)
        datosLab <- datosLab[!is.na(datosLab)]
        mayor = -Inf
        P = 0
        start = 0.001
        end = 0.9
        by = start

        #Para cada probabilidad p
        for (p in seq(start,end,by)){
            dist = NULL # Distribucion para la probabilidad p
            for (i in seq(0,90,10)){
                a = i # valor cubeta inicial
                b = i + 10 # valor cubeta final
                sum = 0 
                for (x in a:b-1){
                    sum = sum + (1-p)^x *p #probabilidad por cubeta
                }
    
                dist = c(dist, sum) # Guardar la suma por cubeta en la districucion
                dist = dist/sum(dist) # normalizar la distribucion
            }

            # realizar la prueba chi de los datos en laboratorio
            # con las probabilidad obtenidas (distribucion)
            #print(length(datosLab))
            prueba = ks.test(datosLab,dist, exact = FALSE) 

            # Si el valor de p de la prueba chi es el mayor ir almacenandolo
            if (prueba$p.value >= mayor){
                if (prueba$p.value >= 0.6){
                    mayor = prueba$p.value
                    P = p
                    Pmejores = c(Pmejores, P)
                    Minutos = c(Minutos, minutos[m])
                }
            }

            
        }
        #cat("Mejor P value",mayor,"\n")
        #cat("Mejor P ",P,"\n")
        PVALUES = c(PVALUES,mayor)
        Ps = c(Ps,P)
        m = m + 1
    
    }
    cat(indice-1,Ps, "\n")
    plot(minutos,Ps, xlab="Minutos", ylab="Probabilidad", main=test)
    plot(Minutos, Pmejores, xlab="Minutos", ylab="Probabilidad", main=test)
    #axis(1, at=minutos, lab=minutos)
    indice = indice + 8
}

warnings()