#Simulacion de caminatas aleatorias
from random import randint, random
from math import pow, ceil
from itertools import product

dimensionMaxima = 2
dimensiones = range(1, dimensionMaxima + 1)
#print dimensiones
maximoDePasos = 2000

repeticiones = 500
sitios = 1024
resultados = dict()

CA = dict()
for dimension in dimensiones:
    #encontrar la potencia mas cercana raiz de la dimension entre el total
    #de sitios
    potencia = int(ceil(pow(sitios, 1.0/dimension)))
    #generar el total de coordenadas que tendra para que sea "renglones"
    #cuadraticos
    renglones = range(potencia)

    #Generar todas las coordenadas que hay en el vector ya sea 1, 2, 3, etc
    #dimensiones.
    coordenadas = list(product(renglones,repeat=dimension))
    
    #Asignar al diccionario con la clave dimension las coordenadas
    #correspondientes de la dimension
    CA[dimension] = coordenadas
    
    #Solamente para la dimension 1 hay que quitarle que sean pares y dejarlo
    #como un vector
    if dimension == 1:
        i = 0
        for c in CA[dimension]:
            CA[dimension][i] = c[-1]
            i+=1
    #print CA[dimension]
    #print len(CA[dimension])



#Funcion de distancia, para encontrar los vecinos
#si la distancia es 1 quiere decir que son vecinos inmediatos
#si la distancia es 2 son los vecinos de las esquinas para dimensiones
#mayores a 1
def distancia(p, q):
    s=0
    try:
        for i in xrange(len(p)):
            s += abs(p[i]-q[i])
    except:
        s+= abs(p-q)
    return s
