#************************************************************************
#*                           PISIS FIME UANL                            *
#************************************************************************
#* Nombre del Programa: XDimensional                                    *
#* Descripcion        : Programa que realiza la simulacion del          *
#*                      automata celular en la aplicacion de procesos   *
#*                      fisicoquimicos                                  *
#*                      Los pasos a seguir son:                         *
#*                      -                                               *
#* Funcional          : Luis Alejandro Benavides Vazquez                *
#* Programador        : Luis Alejandro Benavides Vazquez                *
#* Fecha Creacion     : 16 Febrero del 2015                             *
#************************************************************************
#*                        LOG DE MODIFICACIONES                         *
#************************************************************************
#* Descripcion        :                                                 *
#*                                                                      *
#*                                                                      *
#* Funcional          : Luis Alejandro Benavides Vazquez                *
#* Programador        : Luis Alejandro Benavides Vazquez                *
#* Fecha Creacion     : ? de ? del 2015                                 *
#************************************************************************
#************************************************************************
#&======================================================================
#& Importar archivos y funciones
#&======================================================================

from __future__ import division
import math
from random import random,randint

#Clase que contiene los sitios del automata celular
class sitio(object):
    def _init_(self):
        self.valor = 0
    def h(self):
        self.valor = randint(0,10)
        return self.valor 

class Automata(object):
    def __init__(self, N):
        self.sitios = list()
        self.N = N
        self.M = 0
        for i in xrange(0,N):
            self.sitios.append(sitio())
            self.M += self.sitios[i].h()
            #print self.sitios[i].valor, self.M
        self.hprom = self.M/self.N
        self.x = list()
        self.y = list()

    # Funcion para recibir particulas
    # si h[i+1] y h[i-1] es menor a hprom entonces transfieren todo a h[i]
    def RecibirRegla1(self, actual ,izquierdo , derecho, difIzq=0, difDer=0):
        #print (self.sitios[izquierdo].valor > self.hprom and self.sitios[derecho].valor > self.hprom)
        if (self.sitios[izquierdo].valor > self.hprom and self.sitios[derecho].valor > self.hprom):
            # Recibe de vecino izquierdo si es mayor a hprom
            #print 'Entro'
            difIzq = self.sitios[izquierdo].valor - self.hprom
            self.sitios[actual].valor += difIzq
            #print difIzq
            self.sitios[izquierdo].valor = self.sitios[izquierdo].valor - difIzq

            # Recibe de vecino derecho si es mayor a hprom
            difDer = self.sitios[derecho].valor - self.hprom
            #print difDer
            self.sitios[actual].valor += difDer
            self.sitios[derecho].valor = self.sitios[derecho].valor - difDer
            #print 'R1', self.M
            #print 'R1', self.sitios[actual].valor
        
    # si h[i+1] y h[i-1] es mayor a 0 entonces transfieren todo a h[i]
    def RecibirRegla2(self, actual ,izquierdo, derecho):
        if self.sitios[izquierdo].valor > 0  and self.sitios[derecho].valor > 0:
            # Recibe de vecino izquierdo
            self.sitios[actual].valor += self.sitios[izquierdo].valor
            self.sitios[izquierdo].valor = 0

            # Recibe de vecino derecho
            self.sitios[actual].valor += self.sitios[derecho].valor
            self.sitios[derecho].valor = 0

            #print 'R2', self.sitios[actual].valor

    def Transferir(self, diferencia, actual, izquierdo, derecho):
        # Transfiere a vecino h[i-1]
        if random() < 0.5:
            self.sitios[actual].valor -= diferencia
            self.sitios[izquierdo].valor += diferencia
        else:
            self.sitios[actual].valor -= diferencia
            self.sitios[derecho].valor += diferencia
        #print 'T', self.M
        #print 'T', self.sitios[actual].valor

    def Simulacion(self, tiempoSimulacion, MaxIter, F, V):
        #print 
        for f in F:
            #print f
            for v in V:
                #print v
                for iter in xrange(0, MaxIter):
                    stop = False
                    pasos = 0
                    while not stop:
                        for i in xrange(0,N):
                            vecinoIzquierdo = i - 1
                            vecinoDerecho = i + 1
                            if i == 0: vecinoIzquierdo = self.N - 1
                            if i == self.N - 1: vecinoDerecho = 0

                            #&=====================================================================
                            #& AUTOMATA CELULAR
                            #&=====================================================================
                            #& Reglas automata celular
                            #& 0 - (1 - v)                              -> No cambia
                            #& 1 - (v) P(h <= hPromedio)                -> Recibe regla 1
                            #& 2 - (v) P(h > hPromedio) (f(1-v))        -> Recibe regla 2
                            #& 3 - (v) P(h > hPromedio) (1 - f(1-v))    -> Transfiere
                            #&=====================================================================
                            if (random() < (1-v)):
                                # 1 - v  (No cambia)
                                #h[i] = h[i]
                                continue
                            else:
                                # v (Recibe o transfiere)
                                if (self.sitios[i].valor <= self.hprom):
                                    # h <= hPromedio (Recibe regla 1)
                                    #print 'R1', i, vecinoIzquierdo, vecinoDerecho
                                    self.RecibirRegla1(i, vecinoIzquierdo, vecinoDerecho)
                                    self.M = sum([self.sitios[i].valor for i in xrange(0,self.N)])
                                    #print
                                else:
                                    # hj > h (Recibe regla 2 o Transfiere)
                                    if (random() < f*(1-v)):
                                        # f(1-v)  (Recibe regla 2)
                                        #print 'R2', i, vecinoIzquierdo, vecinoDerecho
                                        self.RecibirRegla2(i, vecinoIzquierdo, vecinoDerecho)
                                        self.M = sum([self.sitios[i].valor for i in xrange(0,self.N)])
                                    else:
                                        # 1 - f(1-v)  (Transfiere)
                                        #Probabilidad de transferir todo o solo una fraccion
                                        #print 'T', i, vecinoIzquierdo, vecinoDerecho
                                        diferencia = self.sitios[i].valor - self.hprom

                                        if (random() <= 0.5):
                                            diferencia = random()*(diferencia)
                                        self.Transferir(diferencia, i, vecinoIzquierdo, vecinoDerecho)
                                        self.M = sum([self.sitios[i].valor for i in xrange(0,self.N)])
                        pasos += 1
                        if pasos == tiempoSimulacion: 
                            stop = True
                            self.x, self.y = self.DistribucionTamannoParticulas()
    
    def DistribucionTamannoParticulas(self):
        # n_bin    Tamano de componentes o bin
        # delta_x  Tamanno de separacion de acuerdo a bin
        # x        Vector con los valores a hacer el histograma
        # y        Vector que contendra al histograma
        T = max([self.sitios[i].valor for i in xrange(0,self.N)])
        DeltaX = 5
        MaxBins = int(T/DeltaX)

        valorBins = list()
    
        frecuenciaTamannoParticulas = list()
    
        for bin in xrange(0,MaxBins):
            frecuenciaTamannoParticulas.append(0)
            valorBins.append(DeltaX*(bin + 1))
            for i in xrange(0,self.N):
                if self.sitios[i].valor >= bin*(DeltaX) and self.sitios[i].valor < bin*(DeltaX) + DeltaX:
                    frecuenciaTamannoParticulas[bin] += 1

        return valorBins, frecuenciaTamannoParticulas
    
    
    def imprime(self):
        for i in xrange(0,len(self.sitios)):
            print self.sitios[i].valor,




#&=====================================================================
#& SIMULACION
#&=====================================================================
#& Variables
#&=====================================================================
#& - N: Numero de sitios
#& - F: valores entre 0.1 y 0.9 con incrementos de 0.1.
#&      Fuerza de atraccion entre particulas
#&
#& - V: valores entre 0.1 y 0.9 con incrementos de 0.1.
#&      Velocidad de propelas
#&
#& - MaxIter: iteraciones por cada combinacion de f y v.
#&
#& - tiempoSimulacion: tiempo permisible para que llegue el automata
#&                     celular a un estado deseado.
#&=====================================================================


#&=====================================================================
#& Declaracion de variables globales
#&=====================================================================
N = 1000

tiempoSimulacion = 100000
MaxIter = 1
F = [x * 0.1 for x in range(5, 6)]
V = [x * 0.1 for x in range(5, 6)]

Dim1 = Automata(N)
Dim1.imprime()
#print
#print Dim1.M

Dim1.Simulacion(tiempoSimulacion, MaxIter, F, V)

#Dim1.imprime()
#print
#print Dim1.M

for i in xrange(0,len(Dim1.x)):
    print Dim1.x[i], Dim1.y[i]

'''
def RegresionLineal (x, y):
    sumx = 0
    sumy = 0
    sumxy = 0
    sumx2 = 0
    
    n = len(x)

    for i in xrange(0,n):
        sumx += x[i]
        sumy += y[i]
        sumxy += x[i] * y[i]
        sumx2 += x[i] * x[i]
    
    #print x
    #print y
    #print 'n: %d, sumx: %d, sumy: %d,sumxy: %d, sumx2: %d' %(n, sumx,sumy,sumxy, sumx2)

    a1 = (n * sumxy - sumx * sumy) / ((n * sumx2 - sumx * sumx))
    a0 = (sumy / n) - a1 * ((sumx / n))

    return a0, a1


def CalculoVarianzaErrorExperimentalContraRegresion(x, y):
    logx = list()
    logy = list()
    logz = list()
    diferencia = list()

    a0 = 0
    a1 = 0
    varianza = 0
    
    media = 0
    sumaError = 0
    sumaDiferencia = 0

    for i in xrange (0,len(x)):
        logx.append(math.log1p(x[i] + 1))
        logy.append(math.log1p(y[i] + 1))
    
    a0, a1 = RegresionLineal(logx, logy)

    for i in xrange (0,len(x)):
        logz.append(a0 + a1 * (x[i]))
        diferencia.append(abs(logy[i] - logz[i]))
        sumaError += diferencia[i]

    media = sumaError/len(diferencia)

    for i in xrange (0,len(diferencia)):
        diferencia.append((diferencia[i]-media) * (diferencia[i]-media))
        sumaDiferencia += diferencia[i]

    varianza = math.sqrt(sumaDiferencia/len(diferencia))
    
    
    return varianza, a0, a1



def CalculoVarianzaErrorSimulacionContraRegresion(x, y, a0, a1):
    logx = list()
    logy = list()
    logz = list()
    diferencia = list()
    
    media = 0
    sumaError = 0
    sumaDiferencia = 0
    
    for i in xrange (0,len(x)):
        logx.append(math.log1p(x[i] + 1))
        logy.append(math.log1p(y[i] + 1))
    
    a0, a1 = RegresionLineal(x, y)

    for i in xrange (0,len(x)):
        logz.append(a0 + a1 * (x[i]))
        diferencia.append(abs(logy[i] - logz[i]))

    media = sumaError/len(diferencia)

    for i in xrange (0,len(diferencia)):
        diferencia.append((diferencia[i]-media) * (diferencia[i]-media))
        sumaDiferencia += diferencia[i]
    
    varianza = math.sqrt(sumaDiferencia/len(diferencia))

    return varianza

'''





